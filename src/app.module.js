import angular from 'angular';

import './services/core.module';
import './components/weatherView';
import './components/forecastView';
import './components/weatherCard';
import './components/lineChart';
import './components/list';

const appModule = angular
  .module('weather-app', [
    'app.core',
    'app.weatherView',
    'app.weatherCard',
    'app.forecastView',
    'app.lineChart',
    'app.list',
  ]);

// Placing the config within a constant and not its
// own file mainly because of the size of the project and
// need to set it up local.
appModule.constant('config', {
  token: '3515f2fbea8202461b64cefdb9c3cf60',
  apiUrl: 'api.openweathermap.org/data/2.5',
  cityList: [
    { name: 'cardiff', code: 'uk' },
    { name: 'london', code: 'uk' },
    { name: 'amsterdam', code: 'nl' },
    { name: 'bruges', code: 'be' },
    { name: 'oslo', code: 'no' },
  ],
  selectionList: [
    {
      first: 'main', second: 'temp', display: 'Temp', metric: '°C',
    },
    {
      first: 'main', second: 'humidity', display: 'Humidity', metric: '%',
    },
    {
      first: 'main', second: 'pressure', display: 'Pressure', metric: 'hPa',
    },
    {
      first: 'wind', second: 'speed', display: 'Wind Speed', metric: 'm/s',
    },
  ],
});

export default appModule;
