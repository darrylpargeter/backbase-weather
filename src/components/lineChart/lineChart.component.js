import template from './lineChart.html';
import controller from './lineChart.controller';

export default {
  template,
  controller,
  bindings: {
    data: '<',
    selection: '<',
  },
};
