import lineChartComponent from './lineChart.component';

const lineChartModule = angular.module('app.lineChart', []);

lineChartModule.component('lineChart', lineChartComponent);

export default lineChartModule;
