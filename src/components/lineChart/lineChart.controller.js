import * as d3 from 'd3';

class LineChartController {
  constructor($element) {
    this.$element = $element[0];
  }

  $onInit() {
    this.setUpDefaultValues();
    this.buildChartData();
    this.render();
  }

  $onChanges() {
    if (this.svg) {
      this.buildChartData();
      this.render();
    }
  }

  setUpDefaultValues() {
    this.margin = {
      top: 20,
      right: 50,
      bottom: 30,
      left: 50,
    };

    this.width = this.$element.offsetWidth - this.margin.left - this.margin.right;
    this.height = this.$element.offsetHeight - this.margin.top - this.margin.bottom;
    this.x = d3.scaleTime().range([0, this.width]);
    this.y = d3.scaleLinear().range([this.height, 0]);
    this.parseTime = d3.timeFormat('%e %b %H:%M');
    this.bisectDate = d3.bisector(d => d.x).left;
    this.valueLine = d3.line()
      .x(d => this.x(d.x))
      .y(d => this.y(d.y));

    this.svg = d3.select('#lineChart').append('svg')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('viewBox', `0 0 ${this.$element.offsetWidth} ${this.$element.offsetHeight}`)
      .attr('preserveAspectRatio', 'xMinYMin');

    this.g = this.svg.append('g')
      .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`);
  }


  render() {
    this.x.domain(d3.extent(this.chartData, d => d.x));
    this.y.domain(d3.extent(this.chartData, d => d.y));

    if (this.g.selectAll('.y.axis').empty()) {
      this.g.append('g')
        .attr('class', 'y axis')
        .call(d3.axisLeft().scale(this.y));
    } else {
      this.g.selectAll('.y.axis')
        .transition().duration(1500)
        .call(d3.axisLeft().scale(this.y));
    }

    this.g.append('g')
      .attr('class', 'x axis')
      .attr('transform', `translate(0, ${this.height})`)
      .call(d3.axisBottom().scale(this.x));

    const lines = this.g.selectAll('.line')
      .data([this.chartData])
      .attr('class', 'line');

    lines.exit()
      .remove();

    lines.enter()
      .append('path')
      .attr('class', 'line')
      .attr('d', this.valueLine)
      .merge(lines)
      .transition()
      .duration(1500)
      .attr('d', this.valueLine);

    this.focus = this.g.append('g')
      .attr('class', 'focus')
      .style('display', 'none');

    this.focus.append('line')
      .attr('class', 'x-hover-line hover-line')
      .attr('y1', 0)

    this.focus.append('line')
      .attr('class', 'y-hover-line hover-line')
      .attr('x1', this.width)
      .attr('x2', -this.width);

    this.focus.append('circle')
      .attr('r', 7.5);

    this.focus.append('text')
      .attr('y', 2)
      .attr('dy', '.31em');

    this.svg.append('rect')
      .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`)
      .attr('class', 'overlay')
      .attr('width', this.width)
      .attr('height', this.height)
      .on('mouseover', () => this.focus.style('display', null))
      .on('mouseout', () => this.focus.style('display', 'none'))
      .on('mousemove', this.mousemove());
  }

  // D3 uses the 'this' keyword to point to the DOM element
  // so need to set up a closure of the mousemove function
  // to provide the class scope as the 'self' variable.
  mousemove() {
    const self = this;

    return function () {
      const x0 = self.x.invert(d3.mouse(this)[0]);
      const i = self.bisectDate(self.chartData, x0, 1);
      const d0 = self.chartData[i - 1];
      const d1 = self.chartData[i];
      const d = x0 - d0.x > d1.x - x0 ? d1 : d0;

      // making sure that the tooltip stays within the the confines of the page
      if (self.width - self.x(d.x) < self.width / 2) {
        self.focus.select('text')
          .attr('x', -170)
          .attr('y', self.height - self.y(d.y));
      } else {
        self.focus.select('text')
          .attr('x', 20)
          .attr('y', self.height - self.y(d.y));
      }

      if (self.height - self.y(d.y) < self.height / 2) {
        self.focus.select('text')
          .attr('y', -20);
      } else {
        self.focus.select('text')
          .attr('y', 20);
      }

      self.focus.attr('transform', `translate(${self.x(d.x)}, ${self.y(d.y)})`);
      self.focus.select('text').text(() => `${self.parseTime(new Date(d.x))} - ${d.y} ${self.selection.metric}`);
      self.focus.select('.x-hover-line').attr('y2', self.height - self.y(d.y));
    };
  }

  buildChartData() {
    this.chartData = this.data.map(points => ({
      x: new Date(points.dt_txt),
      y: +points[this.selection.first][this.selection.second],
    }));
  }
}

LineChartController.$inject = ['$element'];
export default LineChartController;
