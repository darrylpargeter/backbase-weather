import weatherCardComponent from './weatherCard.component';

const weatherCardModule = angular.module('app.weatherCard', []);

weatherCardModule.component('weatherCard', weatherCardComponent);

export default weatherCardModule;
