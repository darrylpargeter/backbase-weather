import template from './weatherCard.html';

export default {
  template,
  bindings: {
    data: '<',
    onSelect: '&',
  },
};
