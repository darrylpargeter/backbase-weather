import template from './forecastView.html';
import controller from './forecastView.controller';

export default {
  template,
  controller,
  bindings: {
    city: '<',
    onSelect: '&',
  },
};
