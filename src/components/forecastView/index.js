import 'angular-animate';

import forecastViewComponent from './forecastView.component';

const forecastViewModule = angular.module('app.forecastView', ['ngAnimate']);

forecastViewModule.component('forecastView', forecastViewComponent);

export default forecastViewModule;
