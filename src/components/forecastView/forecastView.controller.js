class ForecastViewController {
  constructor(config) {
    this.config = config;
  }

  $onInit() {
    [this.selection] = this.selectionList;
    this.active = this.selectionList[0].second;
  }

  onClick(selection, active) {
    this.selection = selection;
    this.active = active;
  }

  get selectionList() {
    return this.config.selectionList;
  }
}

ForecastViewController.$inject = ['config'];

export default ForecastViewController;
