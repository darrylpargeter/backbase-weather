import listComponent from './list.component';

const listModule = angular.module('app.list', []);

listModule.component('list', listComponent);

export default listModule;
