import template from './list.html';

export default {
  template,
  bindings: {
    data: '<',
    active: '<',
    onSelect: '&',
  },
};
