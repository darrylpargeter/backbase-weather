class WeatherViewController {
  constructor(config, WeatherService) {
    this.config = config;
    this.WeatherService = WeatherService;
  }

  $onInit() {
    this.WeatherService.getAllCities()
      .then((res) => {
        this.cities = res;
      });
    this.displayForecast = false;
  }

  onClick(city, code) {
    this.WeatherService.getCityForecast({ name: city, code })
      .then((res) => {
        this.selectedCity = res;
        this.toggleDisplayForecast();
      });
  }

  toggleDisplayForecast() {
    this.displayForecast = !this.displayForecast;
  }
}

WeatherViewController.$inject = ['config', 'WeatherService'];

export default WeatherViewController;
