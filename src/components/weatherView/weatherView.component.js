import template from './weatherView.html';
import controller from './weatherView.controller';

export default {
  template,
  controller,
};
