import weatherViewComponent from './weatherView.component';

const weatherViewModule = angular.module('app.weatherView', []);

weatherViewModule.component('weatherView', weatherViewComponent);

export default weatherViewModule;
