import WeatherService from './weather/weather.service';

const coreModule = angular.module('app.core', []);

coreModule.service('WeatherService', WeatherService);

export default coreModule;
