class WeatherService {
  constructor($http, config, $q) {
    this.$http = $http;
    this.$q = $q;
    this.config = config;
  }

  // Using a Promise.all ($q.all) so that if any requests fail
  // we may still get the other cities data.
  // as with a bulk URL requests if one city fails it returns only
  // an error message not the other citys


  // NOTE: on the console.error in promise catch methods
  // normaly I would pass these thought to an external logging system
  // while handling the error in the front end which in this case I handle
  // the error in the front end just don't pass the error in an external logging
  // system.
  getAllCities() {
    const requestPromiseList = this.buildPromiseArray();

    return this.$q.all(requestPromiseList)
      .then(values => values)
      .catch(error => console.error(error));
  }

  getCityForecast(cityDetails) {
    return this.get('forecast', cityDetails)
      .then(values => values)
      .catch(error => console.log(error));
  }

  get(endpoint, city) {
    const queryString = this.buildQueryString(city);
    const fullUrl = this.buildFullUrl(endpoint, queryString);

    return this.$http.get(fullUrl)
      .then(({ data }) => data)
      .catch(error => console.error(error));
  }

  buildPromiseArray() {
    return this.cityList.map(city => this.get('weather', city));
  }

  buildQueryString(city) {
    return `q=${city.name},${city.code}&appid=${this.token}&units=metric`;
  }

  buildFullUrl(endpoint, queryString) {
    return `https://${this.baseUrl}/${endpoint}?${queryString}`;
  }

  // Giving the config there own getter function
  // means that if how we get the config changes
  // in the future only have to change the element here
  // and not in each function that may call each element
  get cityList() {
    return this.config.cityList;
  }

  get token() {
    return this.config.token;
  }

  get baseUrl() {
    return this.config.apiUrl;
  }
}

WeatherService.$inject = ['$http', 'config', '$q'];

export default WeatherService;
