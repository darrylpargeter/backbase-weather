![Weather app](imgs/weather-app.png)
# Install 
```npm i```

```npm start```

```http://localhost:3000```

# What I have done
The main app is designed following Component-based Architecture utilising the Feature/Presentation design pattern.
There are two feature components within the app ```weatherView``` and ```forecastView``` and three Presentation components ```weatherCard```, ```lineChart``` and ```list```.

The ```weatherView``` is the root component of the app and handles the majority of the UI and Data State.

It starts off by getting the data for each of the five cities as defined by the config constant in the ```app.module.js``` from the ```WeatherService```. It then passes the returned data down into the ```weatherCard``` presentation component which displays the current weather data for each city.

Once the ```weathreCard``` components have been built it lets the users active the ```forecastView``` thought a click event that propagates to the ```weatherView.onClick``` method. that then calls ```WeatherSorvice.getCityForecast``` and toggles the ```displayForecast``` variable that activates the ```ngIf``` statement.

This then shows the second feature component ```forecastView``` that handles the loading of the chart ```lineChart``` and the list of the possible axis to display on the chart list.

The ```forecastView``` handles two click event one that propagates to the ```weatherView``` that will close the ```forecastView```. While the second ```ngClick``` event is handled by the ```forecastView``` itself. this event changes what data is displayed by the ```lineChart```.

  ## Component Layout

  * weatherView

    * forecastView

        * lineChart

        * list

     * weatherCard

# Architectural Choices Considered

## .component() over .directive()
The choice was made to use ```.component()``` over ```.directive()``` to save on boilplate code, and
give access to the components lifeCycle methods e.g. ```$onInit()```, ```$onChanges()```.

## MVVM
I decided not to use MVVM as there was no real need to support bi-directional data binding. 

## Component base Architectural
Going for a component-based architecture felt like the most logical choice as most of the app didn't
need to have any knowledge about the rest of the app and could be built with a basic interface and
made to be reusable if the app ever extended or needed updating to newer versions of angular.

## Use of D3 over other charting liberays
The decision behind D3 was mainly one of familiarity to the library itself.
